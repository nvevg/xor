#include <xor/Exceptions.hh>
#include <xor/AbstractGate.hh>

#include <boost/format.hpp>

using namespace Core;

constexpr const char *INVALID_PIN_ID_MSG = "PinId (%1%, %2%) is invalid";

constexpr const char *INVALID_PIN_MSG = "Pin with PinId (%1%, %2%) is invalid";

constexpr const char *PARSER_ERROR_MSG = "Error at (%1%, %2%) of %3%";

constexpr const char *DOCUMENT_LOADING_EXCEPTION_MSG = "Error while loading document %1%: %2%";

const char *Exception::what() const noexcept {
    return message_.c_str();
}

void Exception::setMessage(const std::string &message) {
    message_ = message;
}

InvalidPinIdException::InvalidPinIdException(PinId invalid): Exception(), id_{invalid}
{
    //auto const messageString = (boost::format(INVALID_PIN_ID_MSG) % id_.GetGate()->GetId() % id_.GetPinNumber()).str();
    //Exception::setMessage(messageString);
}

PinIsNotEmptyException::PinIsNotEmptyException(PinId invalid): Exception(), id_{invalid}
{
   //auto const messageString = (boost::format(INVALID_PIN_MSG) % id_.GetGate()->GetId() % id_.GetPinNumber()).str();
   //Exception::setMessage(messageString);
}

ParserException::ParserException(int line, int column, std::string source = "<stream>"): Exception(),
                                                                                         errorPosition_(std::make_pair(line, column)),
                                                                                         fileName_(source)
{
    auto const message = (boost::format(PARSER_ERROR_MSG) % line % column % source).str();
    Exception::setMessage(message);
}


DocumentLoadingException::DocumentLoadingException(std::string fileName, std::string reasonStr)
{
    auto const message = (boost::format(DOCUMENT_LOADING_EXCEPTION_MSG) % fileName % reasonStr).str();
}
