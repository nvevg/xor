#include <xor/BaseGate.hh>
#include <xor/Exceptions.hh>

#include <boost/uuid/uuid_generators.hpp>
#include <cstdint>
#include <algorithm>
#include <utility>
#include <boost/container_hash/hash.hpp>

using namespace Core;

namespace Core {
std::size_t hash_value(const PinId &id) {
    std::size_t seed = 0x0;
    boost::hash_combine(seed, id.GetGate());
    boost::hash_combine(seed, id.GetPinNumber());

    return seed;
}
}

namespace  {

template <bool IsInput>
void fillPinMap(std::unordered_map<PinId, Pin<IsInput> > &map, std::size_t nPins, AbstractGate *owner) {
    std::size_t pinNumber = 0U;
    std::generate_n(std::inserter(map, map.begin()), nPins, [&pinNumber, owner]() {
        Pin<IsInput> pin(owner, pinNumber++);
        return std::make_pair(pin.GetSelfId(), std::move(pin));
    });
}


}

BaseGate::BaseGate(std::size_t inputCount, std::size_t outputCount):
    inputsCount_{inputCount}, outputsCount_{outputCount}, id_{boost::uuids::random_generator()()}
{
    fillPinMap(inputs_, inputsCount_, this);
    fillPinMap(outputs_, outputsCount_, this);
}

void BaseGate::ConnectTo(PinId outPin, PinId destinationPin) {
    auto &out = getOutput(outPin);
    if (!out.IsEmpty())
        throw PinIsNotEmptyException(out.GetSelfId());

    AbstractGate *destinationGate = destinationPin.GetGate();
    destinationGate->HandleIncomingConnection(destinationPin, outPin);
    out.SetConnectedPinId(destinationPin);

    outputChange_(out.GetSelfId(), destinationPin, Impl::PinChange::Connected);
}

void BaseGate::DisconnectInput(PinId inputPin) {
    auto &pin = getInput(inputPin);
    if (pin.IsEmpty())
        return;

    auto inputId = pin.GetConnectedPinId();
    auto connectedGateId = pin.GetConnectedPinId();
    auto *connectedGate = connectedGateId.GetGate();

    connectedGate->HandleOutputDisconnected(connectedGateId);
    pin.Clear();

    inputChange_(pin.GetSelfId(), inputId, Impl::PinChange::Disconnected);
}

void BaseGate::HandleIncomingConnection(PinId inputPin, PinId connectingPin) {
    auto &pin = getInput(inputPin);
    if (!pin.IsEmpty())
        throw PinIsNotEmptyException(pin.GetSelfId());

    pin.SetConnectedPinId(connectingPin);
    inputChange_(pin.GetSelfId(), connectingPin, Impl::PinChange::Connected);
}

void BaseGate::HandleOutputDisconnected(PinId outputPin) {
    auto &pin = getOutput(outputPin);
    auto inputId = pin.GetConnectedPinId();
    pin.Clear();
    outputChange_(pin.GetSelfId(), inputId, Impl::PinChange::Disconnected);
}

InputPin BaseGate::input(PinId id) const
{
    return getInput(id);
}

OutputPin BaseGate::output(PinId id) const
{
    return getOutput(id);
}

InputPin &BaseGate::getInput(PinId id)
{
    auto itr = inputs_.find(id);
    if (itr == inputs_.end())
        throw InvalidPinIdException(id);

    return itr->second;
}

OutputPin &BaseGate::getOutput(PinId id)
{
    auto itr = outputs_.find(id);
    if (itr == outputs_.end())
        throw InvalidPinIdException(id);

    return itr->second;
}

const InputPin &BaseGate::getInput(PinId id) const
{
    return const_cast<BaseGate *>(this)->getInput(id);
}

const OutputPin &BaseGate::getOutput(PinId id) const
{
    return const_cast<BaseGate *>(this)->getOutput(id);
}

void BaseGate::SetComponentObserver(Impl::IComponentObserver &gate) {
    inputChange_.connect([&gate](PinId p1, PinId p2, Impl::PinChange change) -> void {
        gate.HandleComponentInputChange(p1, p2, change);
    });

    outputChange_.connect([&gate](PinId p1, PinId p2, Impl::PinChange change) -> void {
        gate.HandleComponentOutputChange(p1, p2, change);
    });
}

void BaseGate::RemoveComponentObserver() {
    inputChange_.disconnect_all_slots();
    outputChange_.disconnect_all_slots();
}
