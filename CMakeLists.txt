cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

project(xor)

add_subdirectory(src)
add_subdirectory(tests)
