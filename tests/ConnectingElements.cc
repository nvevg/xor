#include <boost/test/unit_test.hpp>
#include "fakeit.hpp"

#include "ConnectingElements.hh"

#include <xor/StandardGates.hh>
#include <xor/Utils.hh>
#include <xor/IObserver.hh>
#include <xor/Exceptions.hh>

using namespace Core;
using namespace fakeit;

namespace {
constexpr const auto InputNumber = 1;
constexpr const auto OutputNumber = 0;
constexpr const auto PluggableElementInputsNumber = 0;
constexpr const auto PluggableElementOutputsNumber = 2;
constexpr const auto ReceiverElementInputsNumber = 2;
constexpr const auto ReceiverElementOutputsNumber = 0;

class PluggableElement : public BaseGate
{
public:
    PluggableElement(): BaseGate(PluggableElementInputsNumber, PluggableElementOutputsNumber) {}

    void Evaluate() const override {}
};

class ReceiverElement : public BaseGate
{
public:
    ReceiverElement(): BaseGate(ReceiverElementInputsNumber, ReceiverElementOutputsNumber) {}

    void Evaluate() const override {}
};

using ReceiverElementPtr = boost::intrusive_ptr<ReceiverElement>;
using PluggableElementPtr = boost::intrusive_ptr<PluggableElement>;

struct Fixture
{
    Fixture(): APluggableElement{ new PluggableElement }, APlugReceiverElement{ new ReceiverElement },
        InputPinId{ Utils::MakePinId( APlugReceiverElement.get(), InputNumber ) },
        OutputPinId{ Utils::MakePinId( APluggableElement.get(), OutputNumber ) }
    {
        Fake(Method(AnObserver, HandleComponentInputChange));
        Fake(Method(AnObserver, HandleComponentOutputChange));
    }

    PluggableElementPtr APluggableElement;

    ReceiverElementPtr APlugReceiverElement;

    Mock<Impl::IComponentObserver> AnObserver;

    PinId InputPinId;

    PinId OutputPinId;
};

std::function<bool (PinId, PinId, Impl::PinChange)> CreateObservingMatcher(PinId id1, PinId id2, Impl::PinChange expectedChange)
{
    return [id1, id2, expectedChange](PinId v1, PinId v2, Impl::PinChange change) {
        return v1 == id1 && v2 == id2 && change == expectedChange;
    };
}

bool OneElementConnectedToAnother(const PluggableElement& one, const ReceiverElement &another,
                                  PinId output, PinId input) {
    auto const &onesOutput = one.output(output);
    auto const &anothersInput = another.input(input);

    if (onesOutput.IsEmpty() || anothersInput.IsEmpty())
        return false;

    return onesOutput.GetConnectedPinId() == anothersInput.GetSelfId() &&
            anothersInput.GetConnectedPinId() == onesOutput.GetSelfId();
}

}

using namespace fakeit;

BOOST_AUTO_TEST_SUITE(ConnectingElements)

BOOST_FIXTURE_TEST_CASE( CtrInitializesPins, Fixture )
{
    for (std::size_t i = 0; i < PluggableElementOutputsNumber; ++i) {
        auto pin = APluggableElement->output(Utils::MakePinId(APluggableElement.get(), i));
        BOOST_CHECK( pin.GetSelfId().GetPinNumber() == i );
        BOOST_CHECK( pin.GetSelfId().GetGate() == APluggableElement.get() );
    }

    for (std::size_t i = 0; i < ReceiverElementInputsNumber; ++i) {
        auto pin = APlugReceiverElement->input(Utils::MakePinId(APlugReceiverElement.get(), i));

        BOOST_CHECK( pin.GetSelfId().GetPinNumber() == i );
        BOOST_CHECK( pin.GetSelfId().GetGate() == APlugReceiverElement.get() );
    }
}

BOOST_FIXTURE_TEST_CASE( ConnectOneElementToAnother, Fixture )
{
    APluggableElement->ConnectTo(OutputPinId,
                                 InputPinId);

    BOOST_CHECK( OneElementConnectedToAnother( *APluggableElement, *APlugReceiverElement, OutputPinId, InputPinId ) );
}

BOOST_FIXTURE_TEST_CASE( DisconnectOneElementFromAnother, Fixture )
{
    APluggableElement->ConnectTo(OutputPinId, InputPinId);

    BOOST_CHECK( OneElementConnectedToAnother( *APluggableElement, *APlugReceiverElement, OutputPinId, InputPinId ) );

    APlugReceiverElement->DisconnectInput(InputPinId);

    BOOST_CHECK( not OneElementConnectedToAnother( *APluggableElement, *APlugReceiverElement, OutputPinId, InputPinId ) );
}

BOOST_FIXTURE_TEST_CASE( DisconnectInput_DoesNothingIfInputIsEmpty, Fixture )
{
    auto const InputPin = APlugReceiverElement->input( InputPinId );
    BOOST_CHECK( InputPin.IsEmpty() );
    APlugReceiverElement->DisconnectInput( InputPinId );
    auto const InputPin2 = APlugReceiverElement->input( InputPinId );
    BOOST_CHECK( InputPin2.IsEmpty() );
    BOOST_CHECK( InputPin == InputPin2 );
}

BOOST_FIXTURE_TEST_CASE( Throws_PinIsNotEmpty_WhenTryingToConnectToBusyPin, Fixture )
{
    const auto BusyPinId = PinId(APlugReceiverElement.get(), InputNumber);
    APluggableElement->ConnectTo(OutputPinId, BusyPinId);

    AbstractGatePtr gate{ new PluggableElement };
    BOOST_CHECK_THROW( gate->ConnectTo(Utils::MakePinId(gate.get(), OutputNumber), BusyPinId), PinIsNotEmptyException );
}

BOOST_FIXTURE_TEST_CASE( input_Throws_InvalidPinIdException_OnInvalidPinRequest, Fixture )
{
    const auto InvalidInputId = Utils::MakePinId(APluggableElement.get(), 0);

    BOOST_CHECK_THROW( APluggableElement->input(InvalidInputId), InvalidPinIdException );
}

BOOST_FIXTURE_TEST_CASE( output_Throws_InvalidPinIdException_OnInvalidPinRequest, Fixture )
{
    const auto InvalidOutputId = Utils::MakePinId(APluggableElement.get(), PluggableElementOutputsNumber + 1);

    BOOST_CHECK_THROW( APluggableElement->output(InvalidOutputId), InvalidPinIdException );
}

BOOST_FIXTURE_TEST_CASE( NotifiesObserverOnInputChange, Fixture )
{
    const auto ConnectMatcher = CreateObservingMatcher( InputPinId, OutputPinId, Impl::PinChange::Connected );
    const auto DisconnectMatcher = CreateObservingMatcher( InputPinId, OutputPinId, Impl::PinChange::Disconnected );

    APlugReceiverElement->SetComponentObserver( AnObserver.get() );

    APluggableElement->ConnectTo( OutputPinId, InputPinId );
    BOOST_CHECK( Verify( Method( AnObserver, HandleComponentInputChange ).Matching( ConnectMatcher )).Exactly( Once ) );

    APlugReceiverElement->DisconnectInput(InputPinId);
    BOOST_CHECK( Verify( Method( AnObserver, HandleComponentInputChange ).Matching( DisconnectMatcher )).Exactly( Once ) );
}

BOOST_FIXTURE_TEST_CASE( NotifiesObserverOnOutputChange, Fixture )
{
    const auto ConnectMatcher = CreateObservingMatcher( OutputPinId, InputPinId, Impl::PinChange::Connected );
    const auto DisconnectMatcher = CreateObservingMatcher( OutputPinId, InputPinId, Impl::PinChange::Disconnected );

    APluggableElement->SetComponentObserver( AnObserver.get() );

    APluggableElement->ConnectTo( OutputPinId, InputPinId );
    BOOST_CHECK( Verify( Method( AnObserver, HandleComponentOutputChange ).Matching( ConnectMatcher )).Exactly( Once ) );

    APlugReceiverElement->DisconnectInput( InputPinId );
    BOOST_CHECK( Verify( Method( AnObserver, HandleComponentOutputChange ).Matching( DisconnectMatcher )).Exactly( Once ) );
}

BOOST_AUTO_TEST_SUITE_END();
