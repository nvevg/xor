#ifndef LED_PIN_HH
#define LED_PIN_HH

#include <xor/Typedefs.h>
#include <xor/PinId.hh>

#include <cstdint>
#include <array>
#include <xor/arv.hh>

#include <boost/optional.hpp>

namespace Core {
    template <bool IsInput>
    class Pin;
    
    class PinMembers {
    protected:
        PinId m_selfId;

        boost::optional<PinId> m_connectedId;
    public:

        PinMembers(): m_selfId{nullptr, 0} {}

        PinMembers(AbstractGate *gate, PinNumber number): m_selfId(gate, number)
        {
        }

        bool operator==(const PinMembers &rhs) const
        {
            return m_selfId == rhs.m_selfId &&
                    m_connectedId == rhs.m_connectedId;
        }

        void Clear() {
            m_connectedId.reset();
        }

        bool IsEmpty() const { return !m_connectedId; }

        void SetConnectedPinId(PinId id) { m_connectedId = std::move(id); }

        PinId GetSelfId() const { return m_selfId; }

        PinId GetConnectedPinId() const { return *m_connectedId; }
    };

    template<>
    class Pin<true> : public PinMembers {
        using PinMembers::PinMembers;
    };
    
    template<>
    class Pin<false> : public PinMembers {
        using PinMembers::PinMembers;
    };
    
    using InputPin = Pin<true>;
    using OutputPin = Pin<false>;

    template <std::size_t Count>
    using InputPinArray = std::array<InputPin, Count>;

    template <std::size_t Count>
    using OutputPinArray = std::array<OutputPin, Count>;

    using InputPinArrayView = arv::array_view<InputPin>;
    using OutputPinArrayView = arv::array_view<OutputPin>;
}

#endif //	LED_PIN_HH
