#ifndef LED_EXCEPTIONS_HH_
#define LED_EXCEPTIONS_HH_

#include <stdexcept>

#include "PinId.hh"

namespace Core
{

class Exception: public std::exception
{
    const char *what() const noexcept override;

protected:
    void setMessage(const std::string &);

private:
    std::string message_;
};

class InvalidPinIdException: public Exception
{
public:
    InvalidPinIdException(PinId invalid);

    PinId id() const { return id_; }

private:
    PinId id_;
};

class PinIsNotEmptyException: public Exception
{
public:
    PinIsNotEmptyException(PinId invalid);

    PinId id() const { return id_; }

private:

    PinId id_;
};

class DocumentLoadingException : public Exception
{
public:
    DocumentLoadingException(std::string fileName, std::string reasonStr);
};

class ParserException : public Exception {
public:

    ParserException(int line, int column, std::string source);

    inline std::pair<int, int> errorPosition() const noexcept { return errorPosition_; }

    inline const std::string &source() const { return fileName_; }

private:

    std::pair<int, int> errorPosition_;

    std::string fileName_;
};

}
#endif  // LED_EXCEPTIONS_HH_
