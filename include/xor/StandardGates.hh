#ifndef LED_STANDARD_GATES_HH
#define LED_STANDARD_GATES_HH

#include <xor/BaseGate.hh>

namespace Core {

class AndGate final : public BaseGate {
public:
    AndGate(): BaseGate(2, 1) {}
    void Evaluate() const override {}
};

class OrGate final : public BaseGate {
public:
    OrGate(): BaseGate(2, 1) {}
    void Evaluate() const override {}
};

class Bus final : public BaseGate {
public:
    Bus(): BaseGate(1, 1) {
    }

    void Evaluate() const override {}
};

}

#endif
