#ifndef LED_UTILS_HH
#define LED_UTILS_HH

#include <xor/PinId.hh>

#include <algorithm>
#include <boost/variant.hpp>

namespace Core {

class AbstractGate;

namespace Utils {

PinId MakePinId(AbstractGate *gate, PinNumber number);

template <typename Visitor, typename IIter, typename... VisitorArgs>
void ApplyVisitorToRange(IIter b, IIter e, VisitorArgs&&... args) {
    Visitor v{ std::forward<VisitorArgs>(args)... };
    std::for_each(b, e, boost::apply_visitor( v ));
}

}
}
#endif
