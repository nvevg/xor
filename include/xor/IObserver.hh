#ifndef LED_IOBSERVER_HH
#define LED_IOBSERVER_HH
#include <xor/Typedefs.h>
#include <xor/PinId.hh>

namespace Core {

namespace Impl {

enum class PinChange {
    Connected = 0,
    Disconnected,
    MaxChange
};

class IComponentObserver {
public:
    virtual void HandleComponentInputChange(PinId inputId, PinId outputId, PinChange change) = 0;

    virtual void HandleComponentOutputChange(PinId outputId, PinId inputId, PinChange change) = 0;

    virtual ~IComponentObserver() {}
};

}

}
#endif
