﻿#ifndef LED_PIN_ID_HH
#define LED_PIN_ID_HH

#include <xor/Typedefs.h>

namespace Core {

class AbstractGate;

class PinId {
public:
    PinId(AbstractGate *gate, PinNumber number);

    bool operator==(const PinId &right) const {
        return right.m_gate == m_gate &&
                right.m_number == m_number;
    }

    AbstractGate *GetGate() { return m_gate; }

    const AbstractGate *GetGate() const { return m_gate; }

    PinNumber GetPinNumber() const { return m_number; }

private:
    AbstractGate *m_gate;

    PinNumber m_number;
};

}
#endif
