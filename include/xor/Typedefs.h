#ifndef LED_TYPEDEFS_HH
#define LED_TYPEDEFS_HH

#include <cstdint>
#include <boost/uuid/uuid.hpp>
#include <boost/smart_ptr.hpp>
#include <array>
#include <utility>

#include <xor/arv.hh>

namespace Core {
    using PinNumber = std::size_t;
    using GateId = boost::uuids::uuid;

    class AbstractGate;
    using AbstractGatePtr = boost::intrusive_ptr<AbstractGate>;

    class OrGate;
    using OrGatePtr = boost::intrusive_ptr<OrGate>;

    class AndGate;
    using AndGatePtr = boost::intrusive_ptr<AndGate>;

    class Bus;
    using BusPtr = boost::intrusive_ptr<Bus>;

    class Circuit;
    using CircuitPtr = boost::intrusive_ptr<Circuit>;

    class IntegratedCircuit;
    using IntegratedCircuitPtr = std::shared_ptr<IntegratedCircuit>;

    using Position2D = std::pair<int, int>;
    std::size_t hash_value(const Position2D &point);

    class ICircuitLoadingHooks;
    using ICircuitLoadingHooksPtr = std::shared_ptr<ICircuitLoadingHooks>;
}

#endif //	LED_TYPEDEFS_HH
