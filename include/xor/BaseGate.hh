#ifndef LED_BASE_GATE_HH
#define LED_BASE_GATE_HH

#include "AbstractGate.hh"
#include "Typedefs.h"
#include "IObserver.hh"
#include "Pin.hh"
#include "arv.hh"

#include <boost/signals2.hpp>
#include <unordered_map>

namespace Core {

std::size_t hash_value(const PinId &id);

}

namespace std {

template <>
struct hash<Core::PinId> {
    std::size_t operator()(const Core::PinId &id) const {
        // return boost::hash<Core::PinId>()(id);
        return hash_value(id);
    }
};

}

namespace Core {
    class BaseGate: public AbstractGate {
        using SignalToObserver = boost::signals2::signal<void(PinId, PinId, Impl::PinChange)>;

        using InputPinMap = std::unordered_map<PinId, InputPin>;

        using OutputPinMap = std::unordered_map<PinId, OutputPin>;

    public:
        BaseGate(std::size_t inputCount, std::size_t outputCount);

        void ConnectTo(PinId outPin,
                       PinId destinationPin) override;

        void DisconnectInput(PinId inputPin) override;

        GateId GetId() const override { return id_; }

        void SetComponentObserver(Impl::IComponentObserver &gate) override;

        void RemoveComponentObserver() override;

        void HandleIncomingConnection(PinId inputPin, PinId connectingPin) override;

        void HandleOutputDisconnected(PinId inputId) override;

        InputPin input(PinId id) const;

        OutputPin output(PinId id) const;

        ~BaseGate() override {}

    protected:

        BaseGate() = default;

        std::size_t inputsCount_;

        std::size_t outputsCount_;

        InputPinMap inputs_;

        OutputPinMap outputs_;

        InputPin  &getInput(PinId id);

        OutputPin &getOutput(PinId id);

        const InputPin &getInput(PinId number) const;

        const OutputPin &getOutput(PinId number) const;

    private:

        GateId id_;

        SignalToObserver inputChange_;

        SignalToObserver outputChange_;
    };
}

#endif //	LED_BASE_GATE_HH
