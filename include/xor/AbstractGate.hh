#ifndef LED_ABSTRACT_GATE_HH
#define LED_ABSTRACT_GATE_HH

#include <xor/Typedefs.h>
#include <xor/Pin.hh>

#include <boost/smart_ptr.hpp>
#include <boost/smart_ptr/intrusive_ref_counter.hpp>


namespace Core {

namespace Impl {
    class IComponentObserver;
}

    class AbstractGate: public boost::intrusive_ref_counter<AbstractGate>
    {
	public:
        virtual void ConnectTo(PinId outPin, PinId destinationPin) = 0;

        virtual void DisconnectInput(PinId inputPin) = 0;

        virtual GateId GetId() const = 0;

        virtual void HandleIncomingConnection(PinId inPin, PinId connectingPin) = 0;

        virtual void HandleOutputDisconnected(PinId inputId) = 0;

        virtual void SetComponentObserver(Impl::IComponentObserver &observer) = 0;

        virtual void RemoveComponentObserver() = 0;

        virtual ~AbstractGate() {}

    protected:

         virtual void Evaluate() const = 0;
    };
}

#endif //	LED_ABSTRACT_GATE_HH
